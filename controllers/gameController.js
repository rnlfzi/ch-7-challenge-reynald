const { Game, Room, User } = require('../models');

const findWinner = (p1, p2) => {
    if(p1 === 'BATU' && p2 === 'KERTAS') {
        return 'KALAH'
    } else if(p1 === 'BATU' && p2 === 'GUNTING') {
        return 'MENANG'
    } else if(p1 === 'KERTAS' && p2 === 'GUNTING') {
        return 'KALAH'
    } else if(p1 === 'KERTAS' && p2 === 'BATU') {
        return 'MENANG'
    } else if(p1 === 'GUNTING' && p2 === 'BATU') {
        return 'KALAH'
    } else if(p1 === 'GUNTING' && p2 === 'KERTAS') {
        return 'MENANG'
    } else {
        return 'DRAW'
    }
}

const fightRoom = async (req, res) => {
    const {user_id, room_id, choice} = req.body;
    const isNewGame = await Game.findAll({
        where: {
            room_id: room_id,
        }
    })
    const isUserExist = await Game.findOne({
        where: {
            user_id: user_id,
            room_id: room_id
        }
    })
    const findRoom = await Room.findOne({
        where: {
            id: room_id
        }
    })
    const findUser = await User.findOne({
        where: {
            id: user_id
        }
    })
    if(findRoom === null || findUser === null) {
        return res.status(404).json({
            status: 'Gagal membuat Game',
            msg: 'Room atau User tidak di temukan'
        })
    }

    if(isNewGame.length === 0) {
        Game.create({
            user_id: user_id,
            room_id: room_id,
            choice
        })
        .then((response) => {
            res.status(201).json({
                status: 'Yeayyy, Anda berhasil membuat Game',
                data: response
            })
        })
        .catch((err) => {
            res.status(400).json({
                status: 'Gagal membuat game',
                msg: err.message
            })
        })
    } else if(isNewGame.length === 1) {
        if(!isUserExist) {
            const gameResult = findWinner(isNewGame[0].choice, choice)

            Game.create({
                user_id: user_id,
                room_id: room_id,
                choice
            })
            .then(() => {
                Room.update({
                    player: isNewGame[0].user_id,
                    result: gameResult,
                }, {
                    where: {
                        id: room_id
                    }
                })
                .then(() => {
                    if(gameResult === 'DRAW') {
                        res.json({
                            result: 'Yahhh DRAW nih ...'
                        })
                    }
                    res.json({
                        result: `Player 1 ${gameResult}`
                    })
                })
                .catch((err) => {
                    res.status(401).json({
                        status: 'Gagal mengupdate data Game',
                        msg: err.message
                    })
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: 'Gagal membuat Game',
                    msg: err.message
                })
            })
        } else {
            res.status(400).json({
                status: 'Gagal membuat Game',
                msg: 'Pemain ini sudah dimasukan'
            })
        }
    } else {
        res.status(400).json({
            status: 'Gagal membuat Game',
            msg: 'Room sudah di gunakan'
        })
    }
}

module.exports = { fightRoom }
