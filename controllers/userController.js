const { User } = require('../models');

const getAllUser = async (req, res) => {
    try {
        const response = await User.findAll({
            attributes: ['id', 'name', 'username']
        })

        res.status(200).json({
            status: 'Yeayyy, Berhasil memuat semua data User',
            data: response
        })
    } catch (err) {
        res.status(500).json({
            status: 'Gagal untuk memuat semua data User',
            msg: err
        })
    }
}

const getUserById = async (req, res) => {
    try {
        const {id} = req.params
        const response = await User.findOne({
            where: {
                id: id
            },
            attributes: ['id', 'name', 'username']
        })

        if(response === null) {
            return res.status(404).json({
                status: `Yahhh, Maaf User dengan id: ${id} tidak ditemukan`
            })
        }

        res.status(200).json({
            status: `Yeayyy, Berhasil untuk memuat data User berdasarkan id: ${id}`,
            data: response
        })
    } catch (err) {
        res.status(500).json({
            status: `Gagal memuat data User dengan id: ${id}`,
            msg: err.message
        })
    }
}

module.exports = {getAllUser, getUserById}