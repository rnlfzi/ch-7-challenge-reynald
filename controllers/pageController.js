const {User, Room} = require('../models');

const loginPage = (req, res) => {
    const title = 'login';
    res.render('login', {title})
}

const dashboarPage = async (req, res) => {
    const user = await User.findAll()
    const room = await Room.findAll({
        include: ['user']
    })

    res.render('dashboard', {room, user})
}

module.exports = {loginPage, dashboarPage}