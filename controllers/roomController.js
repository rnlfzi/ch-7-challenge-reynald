const { User, Room } = require('../models');

const createRoom = async (req, res) => {
    try {
        const response = await Room.create(req.body);

        res.status(201).json({
            status: 'Yeayyy, Anda berhasil untuk membuat Room',
            data: response
        })
    } catch (err) {
        res.status(500).json({
            status: 'Anda gagal untuk membuat Room',
            msg: err
        })
    }
}

const getRoom = async (req, res) => {
    try {
        const response = await Room.findAll({
            attributes: ['id', 'room_name', 'player', 'result'],
            include: [{
                model: User,
                as: 'user',
                attributes: ['name', 'username']
            }]
        })

        res.status(200).json({
            status: 'Yeayyy, Berhasil untuk memuat data Room',
            data: response
        })
    } catch (err) {
        res.status(500).json({
            status: 'Anda gagal untuk memuat data Room',
            msg: err.message
        })
    }
}

module.exports = { createRoom, getRoom }
