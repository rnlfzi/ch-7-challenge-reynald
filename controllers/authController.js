const { User, Room } = require('../models');

const format = (user) => {
    const {id, username} = user;

    return {
        id,
        username,
        token: user.generateToken(),
    }
}

const login = async (req, res) => {
    try {
        const response = await User.authentication(req.body)
        const {token} = format(response)

        User.update({
            token: token
        }, {
            where: {
                username: req.body.username
            }
        })
       
        return res.redirect('/page/dashboard')
    } catch (err) {
        res.status(401).json({
            status: 'Anda gagal untuk Login',
            msg: err
        })
    }
}

const register = async (req, res) => {
    try {
        return await User.register(req.body)
        .then(() => {
            res.redirect('/page')
        })

    } catch (err) {
        res.status(500).json({
            status: 'Anda gagal untuk Registrasi',
            msg: err
        })
    }
}

const me = (req, res) => {
    const currentUser = req.user;

    res.json(currentUser)
}

module.exports = {login, register, me}