const router = require('express').Router();
const { loginPage, dashboarPage } = require('../controllers/pageController');

router.get('/', loginPage);
router.get('/dashboard', dashboarPage)

module.exports = router;

