const router = require('express').Router();
const { register, login, me } = require('../controllers/authController');
const restrict = require('../middleware/restrict');

router.post('/login', login);
router.post('/register', register);
router.get('/me', restrict, me);

module.exports = router;

