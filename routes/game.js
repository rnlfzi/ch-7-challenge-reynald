const router = require('express').Router();
const { fightRoom } = require('../controllers/gameController');
const restrict = require('../middleware/restrict');

router.post('/', restrict, fightRoom);

module.exports = router;

