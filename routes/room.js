const router = require('express').Router();
const { createRoom, getRoom } = require('../controllers/roomController');
const restrict = require('../middleware/restrict');

router.post('/', restrict, createRoom);
router.get('/', getRoom);

module.exports = router;

