const router = require('express').Router();
const { getAllUser, getUserById } = require('../controllers/userController');
const restrict = require('../middleware/restrict');

router.get('/', restrict, getAllUser);
router.get('/:id', restrict, getUserById);

module.exports = router;

