const router = require('express').Router();
const authRoute = require('./auth');
const userRoute = require('./user');
const roomRoute = require('./room');
const gameRoute = require('./game');
const pageRoute = require('./page');

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/room', roomRoute);
router.use('/game', gameRoute);
router.use('/page', pageRoute);

module.exports = router;

